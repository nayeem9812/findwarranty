<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//All Route For Category
Route::get('/admin/category/index', 'CategoryController@index')->name('category.index');
Route::post('/admin/category/store', 'CategoryController@store')->name('category.store');
Route::get('/admin/category/edit/{category_id}', 'CategoryController@edit')->name('category.edit');
Route::post('/admin/category/update', 'CategoryController@update')->name('category.update');
Route::get('/admin/category/destroy/{category_id}', 'CategoryController@destroy')->name('category.destroy');

//All Route For Product
Route::get('/admin/product/index', 'ProductController@index')->name('product.index');
Route::post('/admin/product/store', 'ProductController@store')->name('product.store');
Route::get('/admin/product/edit/{product_id}', 'ProductController@edit')->name('product.edit');
Route::post('/admin/product/update', 'ProductController@update')->name('product.update');
Route::get('/admin/product/destroy/{product_id}', 'ProductController@destroy')->name('product.destroy');

//All Route For Code
Route::get('/admin/code/index', 'CodeController@index')->name('code.index');
Route::get('/admin/code/category', 'CodeController@getProduct')->name('category.product');
Route::post('/admin/code/store', 'CodeController@store')->name('code.store');

//All Route For Customer
Route::get('/customer/find/warranty', 'CodeController@warrantyIndex')->name('warranty.index');
Route::get('/customer/product/find', 'CodeController@findProduct')->name('product.find');

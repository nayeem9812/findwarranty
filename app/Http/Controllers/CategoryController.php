<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.pages.categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_name' => 'required|unique:categories,category_name',
        ]);

        $category = new Category();
        $category->category_name = $request->category_name;
        $category->save();

        return redirect(route('category.index'))->with('message', 'Category Added Successfully!');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.pages.categories.edit', compact('category'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'category_name' => 'required',
        ]);

        $category = Category::findOrFail($request->id);
        $category->category_name = $request->category_name;
        $category->update();

        return redirect(route('category.index'))->with('message', 'Category Updated Successfully!');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect(route('category.index'))->with('message', 'Category Deleted Successfully!');
    }
}

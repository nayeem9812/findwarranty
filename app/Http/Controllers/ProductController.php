<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $categories = Category::select('id', 'category_name')->get();
        return view('admin.pages.products.index', compact('products', 'categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'product_name' => 'required|unique:products,product_name',
            'warranty_months' => 'required',
        ]);

        $product = new Product();
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->warranty_months = $request->warranty_months;
        $product->save();

        return redirect(route('product.index'))->with('message', 'Product Added Successfully!');
    }

    public function edit($id)
    {
        $categories = Category::select('id', 'category_name')->get();
        $product = Product::findOrFail($id);
        return view('admin.pages.products.edit', compact('categories', 'product'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'product_name' => 'required',
            'warranty_months' => 'required',
        ]);

        $product = Product::findOrFail($request->id);
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->warranty_months = $request->warranty_months;
        $product->update();

        return redirect(route('product.index'))->with('message', 'Product Updated Successfully!');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect(route('product.index'))->with('message', 'Product Deleted Successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Code;
use App\Product;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CodeController extends Controller
{
    public function index()
    {
        $codes = Code::all();
        $categories = Category::select('id', 'category_name')->get();
        return view('admin.pages.codes.index', compact('codes', 'categories'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'category_id' => 'required',
            'product_id' => 'required',
            'unique_code' => 'required|unique:codes,unique_code|min:7|max:7|alpha',
        ]);

        $warrantyTime = Product::where('id', $request->product_id)->select('warranty_months')->first()->warranty_months;
        $code = new Code();
        $code->category_id = $request->category_id;
        $code->product_id = $request->product_id;
        $code->unique_code = $request->unique_code;
        $code->warranty_starts = Carbon::now();
        $code->warranty_ends = Carbon::now()->addDays($warrantyTime*30);
        $code->save();

        return redirect(route('code.index'))->with('message', 'Code Added Successfully!');
    }

    public function getProduct(Request $request)
    {
        if (!$request->categoryId) {
            $html = '<option value="">'.'-- Select Product --'.'</option>';
        } else {
            $html = '<option value="">'.'-- Select Product --'.'</option>';
            $products = Product::where('category_id', $request->categoryId)->get();
            foreach ($products as $product) {
                $html .= '<option value="'.$product->id.'">'.$product->product_name.'</option>';
            }
        }
        return response()->json(['html' => $html]);
    }

    public function warrantyIndex()
    {
        return view('customer.customer');
    }

    public function findProduct(Request $request)
    {
        $product = Code::where('unique_code', $request->code)->first();

        $productInfo = '';
        if($product == null){
            $productInfo .= ' <p>Oops!! Product Not Found With Your Unique Code</p> ';
        }else{
            $productInfo .= '
                <h3>Warranty Information</h3>
                <p>Product Category : <span>'.$product->Category->category_name.'</span></p>
                <p>Product Name : <span>'.$product->Product->product_name.'</span></p>
                <p>Warranted Started : <span>'.$product->warranty_starts.'</span></p>
                <p>Warranty Ends : <span>'.$product->warranty_ends.'</span></p>
            ';

            if(Carbon::parse($product->warranty_ends)->isPast()){
                $productInfo .= '<p class="text-dark"><strong>** Oops!! Your Product Warranty are Expired **</string> </p>';
            }else{
                $productInfo .= '<p class="text-dark"><strong>** Your Product Has Warranty About '.Carbon::parse($product->warranty_ends)->diffForHumans().'  **</string> </p>';
            }
        }

        return response()->json(['productInfo' => $productInfo]);
    }
}

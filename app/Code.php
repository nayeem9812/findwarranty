<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $fillable = [
        'category_id', 'product_id', 'unique_code', 'warranty_starts',
    ];

    public function Category()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function Product()
    {
    	return $this->hasOne('App\Product', 'id', 'product_id');
    }
}

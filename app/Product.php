<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'category_id', 'product_name', 'warranty_months',
    ];

    public function Category()
    {
    	return $this->hasOne('App\Category', 'id', 'category_id');
    }

}

@extends('admin.layouts.master')

@section('title')
    Product Edit
@endsection

@push('css')

@endpush

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif

        <div class="row mt-5">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        Edit Product
                    </div>
                    <div class="card-body">
                        <form action="{{ route('product.update') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <div class="form-group">
                                <label for="category_id" class="col-form-label">Category Name<strong class="text-danger">*</strong></label>
                                <select name="category_id" id="category_id" class="form-control" required>
                                    <option value="">-- Select Category --</option>
                                    @foreach ($categories as $category)
                                        <option {{ $category->id == $product->category_id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>

                                <div class="form-group">
                                    <label for="product_name" class="col-form-label">Product Name <strong class="text-danger">*</strong></label>
                                    <input type="text" name="product_name" id="product_name" class="form-control" value="{{ $product->product_name }}" required>
                                </div>
                                
                                <div class="form-group">
                                    <label for="warranty_months" class="col-form-label">Warranty Months <strong class="text-danger">*</strong></label>
                                    <input type="number" step="any" name="warranty_months" id="warranty_months" class="form-control" value="{{ $product->warranty_months }}" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush


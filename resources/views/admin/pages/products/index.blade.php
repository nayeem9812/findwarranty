@extends('admin.layouts.master')

@section('title')
    Products
@endsection

@push('css')

@endpush

@section('content')
    <div class="container">

        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span></button> <i class="fa fa-info mx-2"></i>
                <strong>{!! session('message') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif

        <div class="row mt-5">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        Add New Product
                    </div>
                    <div class="card-body">
                        <form action="{{ route('product.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="category_id" class="col-form-label">Category Name<strong class="text-danger">*</strong></label>
                                <select name="category_id" id="category_id" class="form-control" required>
                                    <option value="">-- Select Category --</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>

                                <div class="form-group">
                                    <label for="product_name" class="col-form-label">Product Name <strong class="text-danger">*</strong></label>
                                    <input type="text" name="product_name" id="product_name" class="form-control" value="{{ old('product_name') }}" required>
                                </div>
                                
                                <div class="form-group">
                                    <label for="warranty_months" class="col-form-label">Warranty Months <strong class="text-danger">*</strong></label>
                                    <input type="number" step="any" name="warranty_months" id="warranty_months" class="form-control" value="{{ old('warranty_months') }}" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        List of Products
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-stripe table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product Name</th>
                                        <th>Category Name</th>
                                        <th>Warranty Months</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @forelse ($products as $key=>$product)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $product->product_name }}</td>
                                            <td>{{ $product->Category->category_name }}</td>
                                            <td>{{ $product->warranty_months }}</td>
                                            <td>
                                                <a href="{{ route('product.edit', $product->id) }}" class="btn btn-sm btn-info">Edit</a>
                                                <a href="{{ route('product.destroy', $product->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="4" class="text-center">No Product Found!!</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush


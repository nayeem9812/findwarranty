@extends('admin.layouts.master')

@section('title')
    Unique Code Insert
@endsection

@push('css')

@endpush

@section('content')
    <div class="container">

        @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span></button> <i class="fa fa-info mx-2"></i>
                <strong>{!! session('message') !!}</strong>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show rounded" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span></button>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        @endif

        <div class="row mt-5">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        Add New Unique Code
                    </div>
                    <div class="card-body">
                        <form action="{{ route('code.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="category_id" class="col-form-label">Category <strong class="text-danger">*</strong></label>
                                <select name="category_id" class="form-control" id="category_id">
                                    <option value="">-- Select Category --</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="product_id" class="col-form-label">Product <strong class="text-danger">*</strong></label>
                                <select name="product_id" class="form-control products" id="product_id">
                                    <option value="">-- Select Product --</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="unique_code" class="col-form-label">Unique Code <strong class="text-danger">*</strong></label>

                                <div class="input-group">
                                        <input min="7" max="7" type="text" name="unique_code" class="form-control" id="unique_code" placeholder="7 character unique code" value="{{ old('unique_code') }}">                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary generate" type="button">Generate</button>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Save</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        List of Unique Code
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-stripe table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Category</th>
                                        <th>Product</th>
                                        <th>Warranty Start</th>
                                        <th>Warranty End</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @forelse ($codes as $key => $code)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $code->unique_code }}</td>
                                            <td>{{ $code->Category->category_name }}</td>
                                            <td>{{ $code->Product->product_name }}</td>
                                            <td>{{ $code->warranty_starts }}</td>
                                            <td>{{ $code->warranty_ends }}</td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" class="text-center">No Warranty Found!!</td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function makeUniqueCode(length) {
            var result           = '';
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        };

        $('.generate').on('click', function(){
            $('#unique_code').val(makeUniqueCode(7));
        });

        $(document).on("change", "#category_id", function() {
            $.ajax({
                url: "{{ route('category.product') }}?categoryId=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    $('#product_id').html(data.html);
                }
            });
        });

    </script>
@endpush


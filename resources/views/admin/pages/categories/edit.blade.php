@extends('admin.layouts.master')

@section('title')
    Category Edit
@endsection

@push('css')

@endpush

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        Edit Category
                    </div>
                    <div class="card-body">
                        <form action="{{ route('category.update') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" value="{{ $category->id }}">
                            <div class="form-group">
                                <label for="category_name" class="col-form-label">Category Name <strong class="text-danger">*</strong></label>
                                <input type="text" name="category_name" id="category_name" class="form-control" value="{{ $category->category_name }}">
                            </div>
                            <button type="submit" class="btn btn-primary float-right">Update</button>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush


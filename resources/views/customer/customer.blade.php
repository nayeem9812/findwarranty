<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Panacea | Product Warranty</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <!-- Styles -->

    <style>
        .wrapper{
            min-height: 100vh;
            align-items: center;
            display: flex;
            justify-content: center;
            position: relative;
            background: url('/images/bg.jpg');
            background-position: center center;
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
        .image-color-overlay{
            background:rgba(18, 15, 56, 0.85);
            width:100%;
            min-height:100vh;
            display:flex;
            align-items:center;
        }
        .content {
            text-align: center;
        }
        .input-group, .card {
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }
        input, .findButton{
            border-radius: 0 !important;
            font-size:14px;
        }
        input::placeholder{
            font-size:13px;
        }
        input:focus, input:active, .findButton{
            border-color: #e48612 !important;
            box-shadow: none !important;
        }
        .details p{
            margin: 0;
            padding: 0;
            font-size:16px;
        }
        .details p span{
            color:#fff;
            font-size:14px;
        }
        .details h3{
            position:relative;
            padding-bottom:10px;
        }
        .details h3:after{
            content:'';
            position:absolute;
            top:100%;
            left:50%;
            transform:translate(-50%, -50%);
            width:80px;
            height:4px;
            background:#fff;
        }
        .findButton{
            background-color: #e48612;
        }
        .card-body{
            background-color: #e48612;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="image-color-overlay">
            <main class="container content">
                <div class="row">
                    <div class="col-md-6 offset-md-3">
                        <form action="" id="findForm">
                            <div class="input-group">
                                <input type="text" min="7" max="7" class="form-control find" placeholder="Enter Your Unique Warranty Code" required>
                                <div class="input-group-append">
                                    <button class="btn findButton" type="submit">FIND</button>
                                </div>
                            </div>
                        </form>

                        <div class="card mt-3 border-0 d-none">
                            <div class="card-body text-white details">

                            </div>
                        </div>

                    </div>
                </div>
            </main>
        </div>
    </div>

    <!-- Bootstrap JS CDN-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

    <script>
        $('#findForm').submit(function(e){
            e.preventDefault();
            var code = $('.find').val();

            if(code.length != 7){
                $('.details').html('<p>Your Code Should Be 7 Character!</p>')
                $('.card').removeClass('d-none');
            }else{
                $.ajax({
                    url: "{{ route('product.find') }}?code=" + code,
                    method: 'GET',
                    success: function(data) {
                    $('.details').html(data.productInfo);
                    $('.card').removeClass('d-none');
                    }
                });
            }
        });
    </script>
</body>
</html>
